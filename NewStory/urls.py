"""NewStory URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import lab_9.urls as lab_9
import lab_8.urls as lab_8
import lab_5.urls as lab_5
# from lab_1.views import index as index_lab1
#from lab_4.views import mainHome as home



urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include(lab_5,namespace='lab_5')),
    url(r'^lab_8/', include(lab_8,namespace='lab_8')),
    url(r'^lab_9/', include(lab_9,namespace='lab_9')),
    url(r'^auth/', include('social_django.urls', namespace='social')),
]
