from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from django.db import IntegrityError
from . import views
from .models import Schedule, Register
from .forms import ScheduleForm, RegisterForm
import unittest

class Lab10_Test(TestCase):
	def test_lab5_is_exist(self):
		response = Client().get('')
		# pepew = ['pepew3.html', 'starter.html']
		self.assertEqual(response.status_code, 200)
		# self.assertTemplateUsed(response, ", ".join(pepew))

		# response = resolve('')
		# self.assertEqual(response.func, views.mainHome)

	def test_lab5_profile_is_exist(self):
		found = resolve('/profil/')
		# self.assertTemplateUsed(found, 'pepew3_2.html starter.html')
		self.assertEqual(found.func, views.profile)

	def test_lab5_register_is_exist(self):
		found = resolve('/register/')
		# self.assertTemplateUsed(found, 'pepew3_3.html starter.html')
		self.assertEqual(found.func, views.register)

	def test_lab5_schedule_list_is_exist(self):
		found = resolve('/schedule_list/')
		# self.assertTemplateUsed(found, 'pepew3_3.html starter.html')
		self.assertEqual(found.func, views.schedule_list)

	def test_lab5_add_schedule_is_exist(self):
		found = resolve('/add_schedule/')
		# self.assertTemplateUsed(found, 'pepew3_3.html starter.html')
		self.assertEqual(found.func, views.schedule_form)	

	def test_lab5_display_is_exist(self):
		found = resolve('/display/')
		# self.assertTemplateUsed(found, 'pepew3_3.html starter.html')
		self.assertEqual(found.func, views.display_form)

	def test_lab5_schedule_remove_is_exist(self):
		found = resolve('/schedule_remove/')
		# self.assertTemplateUsed(found, 'pepew3_3.html starter.html')
		self.assertEqual(found.func, views.remove_schedule)

	def test_lab5_email_validation_is_exist(self):
		found = resolve('/email_validation/')
		# self.assertTemplateUsed(found, 'pepew3_3.html starter.html')
		self.assertEqual(found.func, views.email_validation)	

	def test_lab5_subscribe_form_is_exist(self):
		found = resolve('/subscribeform/')
		# self.assertTemplateUsed(found, 'pepew3_3.html starter.html')
		self.assertEqual(found.func, views.register_form)



	def test_model_can_create_new_schedule(self):
		day="Minggu"
		date="2012-12-12"
		hour_min="12:12"
		activity="main"
		place="pacil"
		category="penting"

		new_schedule = Schedule.objects.create(day=day, date=date, hour_min=hour_min, activity=activity, place=place, category=category)
		counting_all_schedule = Schedule.objects.all().count()
		self.assertEqual(counting_all_schedule, 1)

	def test_model_can_create_new_subscriber(self):
		new_register = Register.objects.create(name="gusti", email="gusti@yahoo.com", password="gusti1234")
		counting_all_register = Register.objects.all().count()
		self.assertEqual(counting_all_register, 1)

	def test_RegisterForm_valid(self):
		form = RegisterForm(data={'name': "me", 'email': "user@yahoo.com" , 'password': "user1234"})
		self.assertTrue(form.is_valid())

	def test_max_length_name(self):
		name = Register.objects.create(name="paling banyak 30 karakter ya!!")
		self.assertLessEqual(len(str(name)), 30)

	def test_unique_email(self):
		Register.objects.create(email="gusti@email.com")
		with self.assertRaises(IntegrityError):
			Register.objects.create(email="gusti@email.com")

	def test_check_email_view_get_return_200(self):
		email = "gusti@gmail.com"
		Client().post('/email_validation/', {'email': email})
		response = Client().post('/register/', {'email': 'gusti@gmail.com'})
		self.assertEqual(response.status_code, 200)

	def test_check_email_already_exist_view_get_return_200(self):
		Register.objects.create(name="gusti", email="yooo@gmail.com", password="abcde1234")
		response = Client().post('/email_validation/', {
		"email": "yooo@gmail.com"
		})
		self.assertEqual(response.json()['status'], 'fail')

	# def test_subscribe_should_return_status_subscribe_true(self):
	# 	response = Client().post('/subscribeform/', {
	# 	"name": "hmmm",
	# 	"email": "uci@gmail.com",
	# 	"password":  "pass56789",
	# 	})
	# 	self.assertEqual(response.json()['result'], 'Thanks hmmm for subscribe me!!!')

	# def test_subscribe_should_return_status_subscribe_false(self):
	# 	name, email, password = "gusti", "gusti@gmail.com", "password"
	# 	Register.objects.create(name=name, email=email, password=password)
	# 	response = Client().post('/subscribeform/', {
	# 	"name": name,
	# 	"email": email,
	# 	"password":  password,
	# 	})
	# 	self.assertEqual(response.json()['result'], 'fail')



