from django.conf.urls import url
from .views import makeStatus, remove_list

app_name = "lab_8"
urlpatterns = [
	url(r'^$', makeStatus, name='homepage'),
	url(r'home/', makeStatus, name='home'),
	url(r'remove/', remove_list, name="remove"),
]
