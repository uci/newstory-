from django.shortcuts import render, redirect
from .forms import statusForm
from .models import Status
from django.http import HttpResponseRedirect

response = {}

def makeStatus(request):
	form = statusForm(request.POST or None)	
	if(request.method == "POST" and form.is_valid()):
		status = request.POST.get('status')

		Status.objects.create(status=status)
		return redirect("lab_8:homepage")
	# else:
	# 	return HttpResponseRedirect('/')


	Statuses = Status.objects.all().order_by("-id")
	response = {
		"Statuses" : Statuses,
		"form" : form,

	}
	return render(request, 'landing_page.html' , response)

def remove_list(request):
	Statuses = Status.objects.all().delete()
	return render(request, 'landing_page.html', {})