from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
import requests, json
from django.contrib.auth import authenticate, logout
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
# from .models import Searching

response = {}
def getData(request):
	search = request.GET.get("search", "quilting")
	URL = "https://www.googleapis.com/books/v1/volumes?q=" + search
	
	getJson = requests.get(URL).json()
	return JsonResponse(getJson)

def home(request):
	if  request.user.is_authenticated and "fav" not in request.session:
		request.session["email"] = request.user.email
		request.session["sessionid"]=request.session.session_key
		request.session["fav"]=[]

	try:
		if request.session["fav"] is not None:
			response["message"] = len(request.session["fav"])
	except:
		response["message"] = 0

	return render(request, 'book.html', response)

@csrf_exempt
def fav(request):
	if(request.method=="POST"):
		lst = request.session["fav"]
		if request.POST["id"] not in lst :
			lst.append(request.POST["id"])

		request.session["fav"]= lst
		response["message"]=len(lst)
		return JsonResponse(response)

	else:
		return HttpResponse("GET Method not allowed")

@csrf_exempt  
def unfavourite(request):
	if(request.method=="POST"):
		lst = request.session["fav"]
		if request.POST["id"] in lst :
			lst.remove(request.POST["id"])

		request.session["fav"]= lst
		response["message"]=len(lst)  

		return JsonResponse(response)

	else:
		return HttpResponse("GET Method not allowed")

@csrf_exempt
def get_fav(request):
	if request.user.is_authenticated :
		if(request.method=="GET"):
			if request.session["fav"] is not None:
				response["message"] = request.session["fav"]
		else:
			response["message"] = "NOT ALLOWED"
	else:
		response["message"] = ""

	return JsonResponse(response)

def logout_(request):
	messages.info(request, "You're Now Logout....")
	logout(request)
	return HttpResponseRedirect('/lab_9')

