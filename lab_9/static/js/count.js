var counter = 0;
function checkFav(){
    $.ajax({
        type : 'GET',
        url : '/lab_9/get-fav/',
        dataType : 'json',
        success : function(data) {

            for(var i=1; i<=data.message.length; i++) {
                // console.log(data.message[i-1]);
                var id = data.message[i-1];
                var td = document.getElementById(id);
                if(td!=null){
                    td.className='btn btn-sm btn-danger';
                    td.innerHTML = "Disfavourite";
                }  
                $('#counter').html('Favourite count : ' + data.message.length);      
            }
            $('tbody').html(print);
        }
    });  
};

// function signOut() {
//     var auth2 = gapi.auth2.getAuthInstance();
//     auth2.disconnect();
//     auth2.signOut().then(function () {  
//         alert("You're Logout Now");
//     });

// }


function favorite(id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var ini = document.getElementById(id);

    if (ini.className=='btn btn-sm btn-danger') {
        $.ajax({
            url: "/lab_9/unfavourite/",
            type: "POST",
            headers: {"X-CSRFToken": csrftoken},  
            data: {id: id},
            success: function(result) {
                counter=result.message;
                ini.className='btn btn-sm btn-info';
                ini.innerHTML = "Favourite";
                $('#counter').html('Favourite count : ' + counter);
            },
            error : function (errmsg){
                alert("Something is wrong");
            }
        });
    } 

    else {
        $.ajax({
            url: "/lab_9/fav/",
            type: "POST",
            headers: {"X-CSRFToken": csrftoken},  
            data: {id: id},
            success: function(result) {
                counter=result.message;
                ini.className='btn btn-sm btn-danger';
                ini.innerHTML = "Disfavourite";
                $('#counter').html('Favourite count : ' + counter);
               
           },
           error : function (errmsg){
               alert("Something is wrong");
           }
        });
    }
}


// function onSignIn(googleUser){
//     var id_token = googleUser.getAuthResponse().id_token;
//     var data = googleUser.getBasicProfile();
//     console.log('ID     :' + data.getId());
//     console.log('Name   :' + data.getName());
//     console.log('Image  :' + data.getImageUrl());
//     console.log('Email  :' + data.getEmail());

//     var img = data.getImageUrl()
//     $('.greet').text("Hello, " + data.getName());
//     $('.greet').show();
//     $('#gambarProfile').attr('src', img);
//     $('#gambarProfile').show();
//     $('#logout').show();
//     $('.g-signin2').hide();
//     $('#logout').show();

//     $.ajax({
//         url: "/lab_9/",
//     })

// }

// function signOut(){
//     alert("You're Logout Now...");
//     var auth2 = gapi.auth2.getAuthInstance();
//     auth2.signOut().then(function () {
//         console.log('User signed out.');
//         $('.greet').hide();
//         $('#logout').hide();
//         $('.g-signin2').show();
//         $('#gambarProfile').hide();
//     });
// }


$(document).ready(function () {
// ////////////////////////////////////////////////////////////////
  $("input").change(function(){

        var search = $(this).val();
        $("#buton").click(function() {
            // $("table").clear();
            var test = $("#sabeb").val();
            var table = document.getElementById("boddy");
            table.innerHTML = "";
            $.ajax({
                url: "lab_9/data?search=" + test,
                // data: 'json',
                success: function(result){
                    results = result.items; //dalam array
                                    
                    for(var i=0; i<results.length; i++){
                        var tmp ="<td>" + "<img src='" + results[i].volumeInfo.imageLinks.thumbnail+ "'></img></td>" +
                        "<td>" + results[i].volumeInfo.title + 
                        "</td><td>" + results[i].volumeInfo.authors + 
                        "</td><td>" + results[i].volumeInfo.publishedDate +"</td>"+
                        "<td><button class='btn btn-sm btn-info' onclick=favorite(this.id) id="+results[i].id+">Favourite</button></td></tr>";
                    	table.insertAdjacentHTML('beforeend', tmp);
                    }
                    checkFav();
                }
            });
        });
    });
////////////////////////////////////////////////////////////////////////////
});
