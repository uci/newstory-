from django.conf.urls import url
from .views import *

app_name = "lab_9"
urlpatterns = [
	url(r'^$', home, name='homepage'),
	url(r'data/', getData, name='data'),

	url(r'logout/', logout_, name='logout'),

	url(r'fav/', fav, name="favourite"),
    url(r'unfavourite/', unfavourite, name="unfavourite"),
    url(r'get-fav/', get_fav, name="get_favourite"),

]
